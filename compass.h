/*****************************************************/
/** 	File:		Compass.h						**/
/** 	Project:	Albert							**/
/** 	Author:		DDesclaux TFel					**/
/** 	Description: Functions for the compass		**/
/** 	State:		Clean							**/
/*****************************************************/

#ifndef _COMPASS_H_
#define _COMPASS_H_	
	
	#include <Arduino.h>
	#include <Wire.h>
	
	#include "HMC5883L.h"
	#include "casio.h"
	#include "motor.h"
	#include "robot.h"
	#include "define.h"
	
	#define DECLINATION			0.
	#define BUF_READ_COMP		9
	#define COMPASS_FILTRE		1000
	#define NBR_VAL_CALIB		200
	#define DELAY_CALIB			75
	#define ECART_MAX_CALIB		50
	
	void Compass_set();
	void Compass_readAxis(int *xAxis, int *yAxis);
	void Compass_calibration();
	int Compass_getCap();
	int Compass_deltaCap(int value_1, int value_2);

	int minTab(int *tab, int length);
	int maxTab(int *tab, int length);
	int moyTab(int *tab, int length);
	
#endif

