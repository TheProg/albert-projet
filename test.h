/*****************************************************/
/** 	File:		Test.h							**/
/** 	Project:	Albert							**/
/** 	Author:		DDesclaux TFel					**/
/** 	Description: Functions for tests			**/
/** 	State:		Clean							**/
/*****************************************************/

#ifndef _TEST_H_
#define _TEST_H_
	
	#include <Arduino.h>
	
	#include "robot.h"
	#include "define.h"
	#include "motor.h"
	#include "casio.h"
	#include "compass.h"
	
	void Test_demo();
	void Test_compass();
	void Test_compteTour();
	void Test_finCourse();
	void Test_motors();
	void Test_motor(int pinEna);
	
#endif
