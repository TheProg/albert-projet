/*****************************************************/
/** 	File:		Interface.c						**/
/** 	Project:	Albert							**/
/** 	Author:		DDesclaux TFel					**/
/** 	Description: Functions for interfaces		**/
/** 	State:		To do							**/
/*****************************************************/


#include "interface.h"


extern IRrecv irrecv;
extern decode_results results;

extern int tabValues[10];

// IR() -- test -- add remote
// Interface pour les telecommandes infrarouges
void Interface_IR()
{
	char buffer[10];
	
	// On choisit la telecommande et donc ses codes
#ifdef OEM_REMOTE

/*	Mappage de la telecommande OEM
805728D7	805740BF	805748B7
8057B847	80576897	805758A7
80579867	8057F00F	8057A05F
805718E7	8057B04F	805738C7
5936B75E	F1987FCA	4E0CD3B2
F58B082C	B9ABE1E1	8057609F

FIN -> 805740BF
	
avancer -> 80576897
reculer -> 8057B04F

droite -> 80579867
droite av -> 8057B847
droite ar -> 805718E7

gauche -> 8057A05F
gauche av -> 805758A7
gauche ar -> 805738C7

baisser gr -> 8057609F
monter gr -> 4E0CD3B2
*/
	
	#define CODE_FIN		-2141765441	// 0x805740BF
	
	#define CODE_AVANT		0x80576897
	#define CODE_ARRET		0x8057F00F
	#define CODE_ARRIERE	0x8057B04F
	
	#define CODE_VD_SEC		0x80579867
	#define CODE_VD_AV		0x8057B847
	#define CODE_VD_AR		0x805718E7
	
	#define CODE_VG_SEC		0x8057A05F
	#define CODE_VG_AV		0x805758A7
	#define CODE_VG_AR		0x805738C7
	
	#define CODE_GRI_M		-2141751161
	#define CODE_GRI_D		-2141757281

#elif SONY
	
	
	
#endif
	
	// On indique a la Casio ce qu'on fait
	Casio_clear();
	Casio_write_P(PSTR("## INTERFACE IR ##\n"));
	
	// Tant que la touche de fin n'est pas appuyée
	do
	{
		// Si on recoit quelque chose
		if (irrecv.decode(&results))
		{
			// On fait l'action définie
			switch(results.value)
			{
				// Avancer
				case CODE_AVANT:
					Motor_setSens(MOT_NEGATIVE, MOT_NEGATIVE, MOT_BREAK);
					analogWrite(MOT_R_ENA, 255);
					analogWrite(MOT_L_ENA, 255);
					break;
				// Arret
				case CODE_ARRET:
					Robot_arret();
					break;
				// Reculer
				case CODE_ARRIERE:
					Motor_setSens(MOT_POSITIVE, MOT_POSITIVE, MOT_BREAK);
					analogWrite(MOT_R_ENA, 255);
					analogWrite(MOT_L_ENA, 255);
					break;
				// Droite Sec
				case CODE_VG_SEC:
					Motor_setSens(MOT_NEGATIVE, MOT_POSITIVE, MOT_BREAK);
					analogWrite(MOT_R_ENA, 157);
					analogWrite(MOT_L_ENA, 157);
					break;
				// Droite Av
				case CODE_VD_AR:
					Motor_setSens(MOT_BREAK, MOT_POSITIVE, MOT_BREAK);
					analogWrite(MOT_R_ENA, 0);
					analogWrite(MOT_L_ENA, 157);
					break;
				// Droite Av
				case CODE_VD_AV:
					Motor_setSens(MOT_BREAK, MOT_NEGATIVE, MOT_BREAK);
					analogWrite(MOT_R_ENA, 0);
					analogWrite(MOT_L_ENA, 157);
					break;
				// Gauche Sec
				case CODE_VD_SEC:
					Motor_setSens(MOT_POSITIVE, MOT_NEGATIVE, MOT_BREAK);
					analogWrite(MOT_R_ENA, 157);
					analogWrite(MOT_L_ENA, 157);
					break;
				// Droite Av
				case CODE_VG_AR:
					Motor_setSens(MOT_POSITIVE, MOT_BREAK, MOT_BREAK);
					analogWrite(MOT_R_ENA, 157);
					analogWrite(MOT_L_ENA, 0);
					break;
				// Droite Av
				case CODE_VG_AV:
					Motor_setSens(MOT_NEGATIVE, MOT_BREAK, MOT_BREAK);
					analogWrite(MOT_R_ENA, 157);
					analogWrite(MOT_L_ENA, 0);
					break;
				// Monter Gr
				case CODE_GRI_M:
					Robot_grille(GRI_MONTER);
					break;
				// Descendre Gr
				case CODE_GRI_D:
					Robot_grille(GRI_DESCENDRE);
					break;
				case -1:
					break;
			}
			
			// On affiche le code recu
			Casio_write_P(PSTR(" Recu: "));
			sprintf(buffer, "%li", results.value);
			Casio_write(buffer);
			Casio_write_P(PSTR("\n"));
			
			// On se prepare a recevoir la valeur suivante
			irrecv.resume();
		}
	}while(results.value != CODE_FIN);
	
	// On indique qu'on rend la main
	Casio_write_P(PSTR("## FIN IR ##\n"));
}

// BT() -- to test
// Interface for BlueTooth remote
void Interface_BT()
{
	Casio_clear();
	Casio_write_P(PSTR("## INTERFACE BT ##\n"));

#ifndef _BT_

	Casio_write_P(PSTR(" BlueTooth desactivé\nVerifiez si le mode DEBUG n'est pas actif.\n"));
	delay(5000);
	
#else
	
	tabValues[6] = 1;
	while(tabValues[6] != 2)
	{
		Interface_BTEvent();
		
		Motor_setSens(tabValues[0], tabValues[2], tabValues[4]);
		analogWrite(MOT_R_ENA, tabValues[1]);
		analogWrite(MOT_L_ENA, tabValues[3]);
		analogWrite(MOT_G_ENA, tabValues[5]);
	  
		Interface_BTSend();
	}
	
#endif

	Casio_write_P(PSTR("## FIN BT ##\n"));
}

#ifdef _BT_
// serialEvent() -- ok
// Decoder les infos arrivant du BlueTooth
void Interface_BTEvent()
{
	int i, j;
	int cursor=0;
	int inbyte = 0;
	char buffer[100];
	char buf[10];
	
	if(Serial.available())
	{
		Casio_write_P(PSTR("Ordre recu !\n"));
		
		j=0;
		
		String inBytes = Serial.readStringUntil('\n');
		strcpy(buffer, inBytes.c_str());
		Casio_write(buffer);
		Casio_write_P(PSTR("\n\n"));
		
		while(buffer[cursor] != '#')
		{
			i=0;
			while(buffer[cursor] != ':')
			{
				buf[i] = buffer[cursor];
				buf[i+1] = '\0';
				cursor++;
				i++;
			}
			cursor++;
			tabValues[j] = atoi(buf);
			j++;
		}
	}
}

// BTSend() -- ok
// Envoyer les infos au BlueTooth
void Interface_BTSend()
{
	Serial.print(Compass_getCap());
	Serial.print(F(":"));
	Serial.print(0); // CT
	Serial.print(F(":"));
	Serial.print(0); // Radius
	Serial.print(F(":"));
	Serial.print(digitalRead(PIN_FC_H));
	Serial.print(F(":"));
	Serial.print(digitalRead(PIN_FC_B));
	Serial.println(F(":END"));
	delay(DELAY);
}
#endif