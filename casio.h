/*****************************************************/
/** 	File:		Casio.h							**/
/** 	Project:	Albert							**/
/** 	Author:		DDesclaux TFel					**/
/** 	Description: Functions for com with Casio	**/
/** 	State:		Clean							**/
/*****************************************************/

#ifndef _CASIO_H_
#define _CASIO_H_
	
	#include <Arduino.h>
	#include <avr/pgmspace.h>
	#include <SoftwareSerial.h>

	#include "define.h"
	
	void Casio_set();
	char* Casio_read(int type);
	void Casio_write(const char *data);
	void Casio_write_P(const char *data);
	void Casio_wait();
	void Casio_clear();
	
#endif

