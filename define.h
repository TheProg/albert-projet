/*****************************************************/
/** 	File:		Define.h						**/
/** 	Project:	Albert							**/
/** 	Author:		DDesclaux TFel					**/
/** 	Description: Defines of everything			**/
/** 	State:		Clean							**/
/*****************************************************/

#ifndef _DEFINE_H_
#define _DEFINE_H_
	
	#include <avr/pgmspace.h>
	
	
//	#define _DEBUG_
//	#define _DEBUG_COMP_
	
#ifndef _DEBUG_ || _DEBUG_COMP_
	#define _BT_
#endif
	
	#define MODE_1				// Mode of cleaning (Z or squares)
	
	#define ROBOT_LARGE		2	// (cm)
	#define ROBOT_RADIUS	10	// (mm)
	
	// Define constants names
	#define GRI_MONTER		1
	#define GRI_DESCENDRE	2

	#define MOTOR_R			1
	#define MOTOR_L			2
	#define MOTOR_G			3
	#define MOT_POSITIVE	1
	#define MOT_NEGATIVE	2
	#define MOT_BREAK		3
	#define GRI_MONTER		1
	#define GRI_DESCENDRE	2

	// Define Pins
	#define PIN_CT			2	// INT0	-- Wheel
	#define PIN_IR			3	// D3	-- InfraRed
	#define REG_DATA		4	// D4	-- Regiter Data
	#define CASIO_TX		5	// D5	-- Casio TX
	#define CASIO_RX		6	// D6	-- Casio RX
	#define REG_LOCK		7	// D7	-- Register Lock
	#define REG_CLK			8	// D8	-- Register Clock
	#define MOT_R_ENA		9	// D9	-- Right Motor Enable (PWM)
	#define MOT_L_ENA		10	// D10	-- Left Motor Enable (PWM)
	#define MOT_G_ENA		11	// D11	-- Grille Motor Enable (PWM) UNUSED
	#define PIN_FC_H		12	// D12	-- FinCourse Haut
	#define PIN_FC_B		13	// D13	-- FinCourse Bas
	
#endif
