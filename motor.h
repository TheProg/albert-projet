/*****************************************************/
/** 	File:		Motor.h							**/
/** 	Project:	Albert							**/
/** 	Author:		DDesclaux TFel					**/
/** 	Description: Functions tu command motors	**/
/** 	State:		Clean							**/
/*****************************************************/

#ifndef _MOTOR_H_
#define _MOTOR_H_
	
	#include <Arduino.h>
	
	#include "define.h"
	
	void Motor_set();
	void Motor_setSens(int motorR, int motorL, int motorG);

#endif

