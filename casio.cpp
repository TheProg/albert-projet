/*****************************************************/
/** 	File:		Casio.c							**/
/** 	Project:	Albert							**/
/** 	Author:		DDesclaux TFel					**/
/** 	Description: Functions for com with Casio	**/
/** 	State:		ok								**/
/*****************************************************/


#include "casio.h"
SoftwareSerial serialCalc(CASIO_RX, CASIO_TX); // RX, TX


// Set() -- ok
// Initialiser la com entre la Casio et l'Arduino
void Casio_set()
{
	// Vitesse fixée a 9 600 bauds coté CASIO
	serialCalc.begin(9600);
	// On affiche a la Casio qu'on est connécté
	Casio_write_P(PSTR("Arduino connectee.\n"));
	
#ifdef _DEBUG_
	Serial.println(F("\n\t## Casio Comunication Initialized ##\n"));
#endif
}

// Read() -- ok
// Lire les données que nous envoit la Casio avec un filtre
char* Casio_read(int type)
{
	int cursor = 0;
	char buffer[100] = {0};
	char data = 0;
	
#ifdef _DEBUG_
	Serial.print(F("\n# Arduino waiting for datas...\n\t>C:\t"));
#endif
	
	// Tant qu'il n'y a pas de retour a la ligne et que rien n'est écrit
	while(data != '\n'  ||  !cursor)
	{
		// Si la casio envoit quelque chose
		if(serialCalc.available())
		{
			// On le stocke dans data
			data = serialCalc.read();
			
#ifdef _DEBUG_
	Serial.print(data);
#endif
			
			// Si on attend un chiffre et que s'en est un
			if(type == 0	&&	data>='0' && data<='9'	&&	cursor<100)
			{
				// On le stocke dans le buffer de retour
				buffer[cursor] = data;
				cursor++;
			}
			// Si c'est une lettre et qu'on en attend une
			if(type == 1	&&	(data>='A' && data<='Z'	||	data>='a' && data<='z')	&&	cursor<100)
			{
				// On la stocke dans le buffer de retour
				buffer[cursor] = data;
				cursor++;
			}
		}
	}
	
#ifdef _DEBUG_
	Serial.print(F("\t>C final:\t"));
	Serial.println(buffer);
#endif
	
	// On renvoit le buffer
	return buffer;
}

// Write() -- ok
// Envoyer des donnés a la Casio
void Casio_write(const char *data)
{
	// On ecrit la chaine sur le Serial
	serialCalc.write(data);
	
#ifdef _DEBUG_
	Serial.println(F("\n# Arduino sending to Casio..."));
	Serial.print(F("\t>A:\t"));
	Serial.println(data);
#endif
}

// Write_P() -- ok
// Envoyer des données a la Casio depuis la Flash
void Casio_write_P(const char *data)
{
	// Tant qu'on est pas a la fin de la chaine
	while(pgm_read_byte(data) != 0)
	{
		// On envoit le byte de la memoire
		serialCalc.write(pgm_read_byte(data++));
	}
}

// Wait() -- ok
// Attendre que quelque chose arrive sur le serie de la Casio
void Casio_wait()
{
	while(!serialCalc.available())
		delay(100);
	while(serialCalc.available())
		delay(100);
}

// Clear() -- ok
// Effacer l'ecran de la Casio
void Casio_clear()
{
	// On fait des sauts a la ligne
	Casio_write_P(PSTR("\n\n\n\n\n\n\n\n\n"));
}

