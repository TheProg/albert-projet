/*****************************************************/
/** 	File:		Motor.c							**/
/** 	Project:	Albert							**/
/** 	Author:		DDesclaux TFel					**/
/** 	Description: Functions tu command motors	**/
/** 	State:		ok								**/
/*****************************************************/


#include "motor.h"

// Set() -- ok
// Initialiser les broches des moteurs
void Motor_set()
{
	Motor_setSens(MOT_BREAK, MOT_BREAK, MOT_BREAK);
	
	analogWrite(MOT_G_ENA, 0);
	analogWrite(MOT_R_ENA, 0);
	analogWrite(MOT_L_ENA, 0);
}

// SetSens() -- ok
// Envoyer les etats aux registres pour les hacheurs
void Motor_setSens(int motorR, int motorL, int motorG)
{
	// Le mot binaire est stocké dans la variable "state"
	int state = 0b00000000;
	
	// On regle l'etat des broches pour le moteur droit
	switch(motorR)
	{
		// Si on veut tourner en positif
		//	Alors la broche 1 est a 0V et la broche 2 a 5V
		case MOT_POSITIVE:
			state += 0b01000000;
			break;
		// Sinon si on veut tourner en negatif
		//	Alors la broche 1 est a 5V et la broche 2 a 0V
		case MOT_NEGATIVE:
			state += 0b10000000;
			break;
		// Si on veut etre en mode frein
		//	Alors la broche 1 est a 0V et la broche 2 a 0V
		case MOT_BREAK:
			state += 0b00000000;
			break;
	}
	// Meme chose qu'au dessus pour le moteur gauche
	switch(motorL)
	{
		case MOT_POSITIVE:
			state += 0b00010000;
			break;
		case MOT_NEGATIVE:
			state += 0b00100000;
			break;
		case MOT_BREAK:
			state += 0b00000000;
			break;
	}
	// Meme chose qu'au dessus pour le moteur de la grille
	switch(motorG)
	{
		case MOT_POSITIVE:
			state += 0b00000100;
			break;
		case MOT_NEGATIVE:
			state += 0b00001000;
			break;
		case MOT_BREAK:
			state += 0b00000000;
			break;
	}
	
#ifdef _DEBUG_
//	Serial.print(F("Motor Sens Value:\t"));
//	Serial.println(state);
#endif
	
	// On bloque l'etat des broches actuel
	digitalWrite(REG_LOCK, LOW);
	// On envoit les nouveaux etats
	shiftOut(REG_DATA, REG_CLK, MSBFIRST, state);
	// On met a jour l'etat des broches
	digitalWrite(REG_LOCK, HIGH);
}

