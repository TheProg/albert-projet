/**
	ToDo list
		- Refaire les virages -> Done a tester
		- inclure IR -> Done
		- Check CT et FC -> FC fait, CT a revoir
		- ENA grille ?
		- Checker avec les moteurs -> fait
			-Checker aller tout droit (derive a gauche)
			-Cahnger positif et negatif
			-Checker virage
			-Checker boussole
		- Faire mode 2
	
	SDA -> A4
	SCL -> A5
	
	IN 1	IN 2	SENS
	0		0		FREIN
	0		1		HORAIRE
	1		0		ANTI-HORAIRE
	1		1		FREIN
**/

#include <avr/pgmspace.h>
#include <Wire.h>
#include <IRremote.h>
#include <SoftwareSerial.h>

#include "define.h"
#include "casio.h"
#include "compass.h"
#include "interface.h"
#include "motor.h"
#include "robot.h"
#include "test.h"

typedef enum {MAIN = 0, NETTOYAGE, INTERFACES, TESTS} MenuType;

typedef struct
{
	int size[2];
	int direction[4];
} Plage;

IRrecv irrecv(PIN_IR);
decode_results results;
int tabValues[10];

volatile int nbrTurns;


void setup()
{
	Serial.begin(9600);
	
	// Registre
	pinMode(REG_CLK, OUTPUT);
    pinMode(REG_DATA, OUTPUT);
    pinMode(REG_LOCK, OUTPUT);
	
	// FC
    pinMode(PIN_FC_H, INPUT);
    pinMode(PIN_FC_B, INPUT);
	
	// Compte Tour
	attachInterrupt(0, Inter_CT, RISING);
	
	// Communication
	irrecv.enableIRIn();
	Casio_set();
	
	// Moteurs
	Motor_set();
	Robot_arret();
	
	// Compas
	Compass_set();
//	Robot_grille(GRI_MONTER);
	Compass_calibration();
//	Robot_grille(GRI_DESCENDRE);
	
	delay(5000);
}

void loop()
{
	MenuType menu = MAIN;
	int choix = 0;
	
	while(1)
	{
		Robot_arret();
		if(menu == MAIN)
		{
			Casio_clear();
			Casio_write_P(PSTR("IU Albert 2.0\n"));
			Casio_write_P(PSTR("\nMenu Principal:\n"));
			Casio_write_P(PSTR("  1: Lancer nettoyage\n"));
			Casio_write_P(PSTR("  2: Interfaces\n"));
			Casio_write_P(PSTR("  3: Tests\n"));
			
			menu = (MenuType)atoi(Casio_read(0));
		}
		else if(menu == NETTOYAGE)
		{
			Casio_clear();
			Casio_write_P(PSTR("IU Albert 2.0\n"));
			Casio_write_P(PSTR("\nNettoyage:\n"));
			Casio_write_P(PSTR("  1: Commencer !\n"));
			Casio_write_P(PSTR("  2: Retour\n"));
			
			choix = atoi(Casio_read(0));
			
			switch(choix)
			{
				case 1:
					Clean_main();
					break;
				default:
					menu = MAIN;
			}
		}
		else if(menu == INTERFACES)
		{
			Casio_clear();
			Casio_write_P(PSTR("IU Albert 2.0\n"));
			Casio_write_P(PSTR("\Interfaces:\n"));
			Casio_write_P(PSTR("  1: Interface BT\n"));
			Casio_write_P(PSTR("  2: Interface IR\n"));
			Casio_write_P(PSTR("  3: Demo\n"));
			Casio_write_P(PSTR("  4: Retour\n"));
			
			switch(atoi(Casio_read(0)))
			{
				case 1:
					Interface_BT();
					break;
				case 2:
					Interface_IR();
					break;
				case 3:
					Test_demo();
					break;
				default:
					menu = MAIN;
			}
		}
		else if(menu == TESTS)
		{
			Casio_clear();
			Casio_write_P(PSTR("IU Albert 2.0\n"));
			Casio_write_P(PSTR("\nTests:\n"));
			Casio_write_P(PSTR("  1: Bousolle\n"));
			Casio_write_P(PSTR("  2: Compte tour\n"));
			Casio_write_P(PSTR("  3: Fins de course\n"));
			Casio_write_P(PSTR("  4: Moteurs\n"));
			Casio_write_P(PSTR("  5: Retour\n"));
			
			switch(atoi(Casio_read(0)))
			{
				case 1:
					Test_compass();
					break;
				case 2:
					Test_compteTour();
					break;
				case 3:
					Test_finCourse();
					break;
				case 4:
					Test_motors();
					break;
				default:
					menu = MAIN;
			}
		}
	}
}


// ## CLEANING FUNCTIONS ##
void Clean_main()
{
	char buffer[25];
	int nbrValues = 10;
	Plage plage;
	
	Casio_clear();
	Casio_write_P(PSTR("Nettoyage de plage\n"));
	Casio_write_P(PSTR(" Dimensions surface:\n"));
	Casio_write_P(PSTR("  Largeur(dm): "));
	plage.size[0] = atoi(Casio_read(0));
	Casio_write_P(PSTR("  Longueur(dm): "));
	plage.size[1] = atoi(Casio_read(0));
	
	Casio_clear();
	Casio_write_P(PSTR("Nettoyage de plage\n"));
	Casio_write_P(PSTR(" Reglage Boussole:\n"));
	
	Casio_write_P(PSTR("  Nbr de valeurs:"));
	nbrValues = atoi(Casio_read(0));
	nbrValues %= 999;
	
	Casio_clear();
	Casio_write_P(PSTR("Mettez le robot parallelement a la longueur de la plage et envoyez 1.\n"));
	while(atoi(Casio_read(0)) != 1);
	
	Casio_write_P(PSTR("\nReglage en cours.\n"));
	Casio_write_P(PSTR("Patientez...\n"));
	
	int tab[nbrValues];
	for(int i=0; i<nbrValues; i++)
	{
		tab[i] = Compass_getCap();
#ifdef _DEBUG_
		Serial.print(F("Set Cap 1: tab["));
		Serial.print(i);
		Serial.print(F("] = "));
		Serial.print(tab[i]);
		sprintf(buffer, "\n Cap %03d/%03d: %03d", i, nbrValues, (int)tab[i]);
		Casio_write(buffer);
#endif
		delay(400);
	}
	plage.direction[0] = moyTab(tab, nbrValues);
	plage.direction[1] = plage.direction[0]+90;
	plage.direction[2] = plage.direction[1]+90;
	plage.direction[3] = plage.direction[2]+90;
	
	Casio_clear();
	Casio_write_P(PSTR("Nettoyage de plage\n"));
	Casio_write_P(PSTR(" Pret a partir !\n Appuyez sur 1 pour lancer capitaine.\n"));
	while(atoi(Casio_read(0)) != 1);
	
	/*
		Mode 1:
			Avancer de la longueur direction 1
			Tourner de 90 direction 2
			Avancer de (largeur/tailleRobot)/2 direction 2
			Tourner de 90 direction 3
			Avancer de la longueur direction 3
			Tourner de 90 direction 4
			Avancer de (largeur/tailleRobot)/2-1 direction 4
			
		Mode 2:
			Avancer de la longueur direction 1
			Tourner de 90 direction 2
			Avancer de tailleRobot direction 2
			Tourner de 90 direction 3
			Avancer de la longueur direction 3
			Tourner de -90 direction 2
			Avancer de tailleRobot direction 2
			Tourner de -90 direction 2
			Avancer de la longueur direction 1
	*/
	
	Casio_write_P(PSTR("## DEBUT CYCLE ##\n"));
#ifdef MODE_1
	int nbrPassages=0;
	
	do
	{
		Robot_tourner(plage.direction[0], 1);
		Robot_avancerTmp(plage.direction[0], plage.size[0]);
		Robot_tourner(plage.direction[1], 1);
		Robot_avancerTmp(plage.direction[1], ROBOT_LARGE);
		Robot_tourner(plage.direction[2], 1);
		Robot_avancerTmp(plage.direction[2], plage.size[0]);
		Robot_tourner(plage.direction[1], 1);
		Robot_avancerTmp(plage.direction[1], ROBOT_LARGE);
		
		nbrPassages+=2;
	}while(ROBOT_LARGE * nbrPassages < plage.size[0]);
#else
	
#endif
	Casio_write_P(PSTR("## FIN CYCLE ##"));
}


// ## INTERUPT FUNCTION ##
void Inter_CT()
{
	nbrTurns++;
}

