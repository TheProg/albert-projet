/*****************************************************/
/** 	File:		Compass.c						**/
/** 	Project:	Albert							**/
/** 	Author:		DDesclaux TFel					**/
/** 	Description: Functions for the compass		**/
/** 	State:		ok								**/
/*****************************************************/


#include "compass.h"

HMC5883L compass;
float offset_x=0.;
float offset_y=0.;


// Set() -- ok
// Initialise le compas
void Compass_set()
{
	int error; 
	
	// On affiche sur la Casio qu'on initialise
	Casio_write_P(PSTR("Initilisation Bousolle\n"));
	delay(250);
	
	// On regle l'echelle de mesure
	error = compass.SetScale(1.3);
#ifdef _DEBUG_
	if(error != 0) Serial.println(compass.GetErrorText(error)); // Si il y a une erreur alors on l'affiche
#endif
	
	// On regle le mode de mesure sur "continu"
	error = compass.SetMeasurementMode(Measurement_Continuous);
#ifdef _DEBUG_
	if(error != 0) Serial.println(compass.GetErrorText(error)); // Si il y a une erreur alors on l'affiche
#endif
	
	// On affiche sur la Caiso que l'initialisation est terminée
	Casio_write_P(PSTR("Succes.\n"));
	delay(500);
}

// ReadAxis -- ok
// Lit les axes x et y renvoyés par la boussole
void Compass_readAxis(int *xAxis, int *yAxis)
{	
	int buf[2][BUF_READ_COMP] = {0};
	int x, y;
	int i, j, k;
	
	i=0;
	do
	{
		// On lit la valeur brute
		MagnetometerScaled scaled = compass.ReadScaledAxis();
		x = (int)(scaled.XAxis-offset_x + .5);
		y = (int)(scaled.YAxis-offset_y + .5);
		
		// Si les valeurs de x et y sont comprises dans l'intervalle de coherance
		//	Alors on les palce dans l'ordre croissant dans buf
		if(y<COMPASS_FILTRE && y>-COMPASS_FILTRE && x<COMPASS_FILTRE && x>-COMPASS_FILTRE)
		{
			// X
			k=i;
			for(j=0; j<i; j++)
			{
				if(x < buf[0][j])
				{
					k=j;
					break;
				}
			}
			
			for(j=BUF_READ_COMP-1; j>k; j--)
				buf[0][j] = buf[0][j-1];
			
			buf[0][k] = x;
			
			// Y
			k=i;
			for(j=0; j<i; j++)
			{
				if(y < buf[1][j])
				{
					k=j;
					break;
				}
			}

			for(j=BUF_READ_COMP-1; j>k; j--)
				buf[1][j] = buf[1][j-1];

			buf[1][k] = y;
			
			i++;
		}
	}while(i < BUF_READ_COMP);
	
	// On prend les valeurs du milieu pour enlever les valeurs aberantes
	*xAxis = buf[0][(int)(BUF_READ_COMP/2)];
	*yAxis = buf[1][(int)(BUF_READ_COMP/2)];
}

// Calibration() -- ok
// Calibre automatiquement la boussole
void Compass_calibration()
{
	int tab[2][NBR_VAL_CALIB];
	char buffer[50];
	
#ifdef _DEBUG_
	Serial.println(F("Calibration de la boussole..."));
#endif
	// On affiche sur la Casio ce que l'on fait
	Casio_clear();
	Casio_write_P(PSTR("Calibration boussole\n"));
	Casio_write_P(PSTR(" Patientez...\n"));
	
	// On fait tourner sur lui meme le robot
	Motor_setSens(MOT_POSITIVE, MOT_NEGATIVE, MOT_BREAK);
	analogWrite(MOT_R_ENA, 157);
	analogWrite(MOT_L_ENA, 157);
	
	int i=0;
	// On remplit le tableau tab avec les valeurs de x et y 
	do
	{
		Compass_readAxis(&tab[0][i], &tab[1][i]);
		
#ifdef _DEBUG_
		Serial.print(F("X:\t"));
		Serial.print(tab[0][i]);
		Serial.print(F("\tY:\t"));
		Serial.print(tab[1][i]);
		Serial.print(F("\tDelta:\t"));
		Serial.println(abs(tab[0][0]-tab[0][i-1])+abs(tab[1][0]-tab[1][i-1]));
#endif

		i++;
		
		delay(DELAY_CALIB);
	}while(i<NBR_VAL_CALIB);
	
	// On s'arrete de tourner
	Robot_arret();
	
	// On calcule les offsets
	offset_x = (float)(minTab(tab[0], i) + maxTab(tab[0], i))/2.;
	offset_y = (float)(minTab(tab[1], i) + maxTab(tab[1], i))/2.;
	
#ifdef _DEBUG_
	Serial.println(F("\n\n----------------------------"));
	Serial.println(F("Resultats de la calibration:"));
	Serial.print(F("\tOffset sur x:\t"));
	Serial.println(offset_x);
	Serial.print(F("\tOffset sur y:\t"));
	Serial.println(offset_y);
	Serial.println();
	
	delay(5000);
#endif
	
	// On affiche le resultat sur la Casio
	Casio_write_P(PSTR(" Termine !\n"));
	sprintf(buffer, "x: %d  y: %d", (int)offset_x, (int)offset_y);
	Casio_write(buffer);
}

// getCap() -- ok
// Renvoit la valeur du cap
int Compass_getCap()
{
	static float previousHeading = 0;
	static float offset = 0;
	int x, y;
	float heading;
	
	// On lit la valeur des axes
	Compass_readAxis(&x, &y);
	
	// On calcule le cap a partir de ces valeurs
	heading = atan2((float)y, (float)x)*180./M_PI;
	
#ifdef _DEBUG_COMP_
	Serial.print(F("X:\t"));
	Serial.print(x);
	Serial.print(F("\tY:\t"));
	Serial.print(y);
	Serial.print(F("\tCAP:\t"));
	Serial.println(heading);
#endif
	
	// On corrige si le signe est negatif
	if(heading < 0.)
		heading += 360.;

	// On corrige si le cap est superieur a 360
	if(heading > 360.)
		heading -= 360.;
	
	// On calcule le decalage
	if(previousHeading - heading > 180.)
		offset ++;
	if(previousHeading - heading < -180.)
		offset --;
	
	previousHeading = heading;
	// On lisse la valeur du cap par rapport a la precedante
	heading += offset*360.;
	
#ifdef _DEBUG_COMP_
	Serial.print(F("Cap:\t")); Serial.print(previousHeading); Serial.print(F(" : ")); Serial.println(heading);
#endif
	
	return (int)(heading+.5);
}

// DeltaCap() -- ok
// Renvoit le delta entre les deux cap sur -180 : +180
int Compass_deltaCap(int value_1, int value_2)
{
	int i=0;
	
	while(value_1-(value_2+360*i) >= 180)
		i++;
	while(value_1-(value_2+360*i) <= -180)
		i--;
	
	return value_1-(value_2+360*i);
}

// minTab() -- ok
// Renvoit la valeur minimale d'un tableau
int minTab(int *tab, int length)
{
	int min = 0;
	
	for(int i=0; i<length; i++)
	{
		if(min > tab[i])
			min = tab[i];
	}
	
	return min;
}

// maxTab() -- ok
// Renvoit la valeur minimale d'un tableau
int maxTab(int *tab, int length)
{
	int max = 0;

	for(int i=0; i<length; i++)
	{
		if(max < tab[i])
			max = tab[i];
	}
	
	return max;
}

// moyTab() -- ok
// Renvoit la valeur moyenne d'un tableau
int moyTab(int *tab, int length)
{
	int sum = 0;
	
	for(int i=0; i<length; i++)
		sum += tab[i];
	
	return sum/length;
}

