/*****************************************************/
/** 	File:		Compass.c						**/
/** 	Project:	Albert							**/
/** 	Author:		DDesclaux TFel					**/
/** 	Description: Functions for the compass		**/
/** 	State:		ok								**/
/*****************************************************/


#include "robot.h"

extern int nbrTurns;

// Avancer() -- ok
// Faire avancer le robot dans une direction sur une distance
void Robot_avancer(int consigne, int length)
{
	char buffer[30];
	
	// On affiche ce qu'on va faire sur la Casio
	sprintf(buffer, " Avancer cap %d sur %d\n", consigne, length);
	Casio_write(buffer);
	
	// On initialise la variable comptant les tours de roue
	nbrTurns = 0;
	
#ifdef _DEBUG_
		Serial.print(F("# Avancer cap:\t"));
		Serial.println(consigne);
		Serial.print(F("\tDistance a parcourir:\t"));
		Serial.println(nbrTurns*ROBOT_RADIUS-length*100);
#endif
	
	// Tant que la distance parcourue est inferieure a la consigne
	while(nbrTurns*ROBOT_RADIUS < length*100)
	{
		int cap = 0;
		cap = Compass_getCap();
		
#ifdef _DEBUG_
			Serial.print(F("\tConsigne:\t")); Serial.print(consigne);
			Serial.print(F("\tCap:\t")); Serial.print(cap);
			Serial.print(F("\tDeltaCap:\t")); Serial.print(Compass_deltaCap(cap, consigne));
			Serial.print(F("\tDeltaCap<0:\t")); Serial.print(Compass_deltaCap(cap, consigne) < 0);
			Serial.print(F("\tDeltaCap>0:\t")); Serial.print(Compass_deltaCap(cap, consigne) > 0);
			Serial.print(F("\tDistance:\t")); Serial.print(nbrTurns*ROBOT_RADIUS - length*100);
			Serial.println();
#endif
		
		// On fait tourner les deux moteurs dans le bon sens
		// (modification de l'etat des broches du hacheur)
		Motor_setSens(MOT_NEGATIVE, MOT_NEGATIVE, MOT_BREAK);
		
		// Si l'ecart entre le cap et la consigne est negatif
		//	Alors on tourne vers la gauche (ralentissement moteur gauche)
		if(Compass_deltaCap(cap, consigne) < 0)
		{
#ifdef _DEBUG_
			Serial.print(F("\tDeltaCap < 0:\t"));
			Serial.println(Compass_deltaCap(cap, consigne));
#endif
			
			analogWrite(MOT_L_ENA, 157);
			analogWrite(MOT_R_ENA, 255);
		}
		// Sinon si l'ecart entre le cap et la consigne est positif
		//	Alors on tourne vers la droite (ralentissement moteur droit)
		else if(Compass_deltaCap(cap, consigne) > 0)
		{
#ifdef _DEBUG_
			Serial.print(F("\tDeltaCap > 0:\t"));
			Serial.println(Compass_deltaCap(cap, consigne));
#endif
			
			analogWrite(MOT_L_ENA, 255);
			analogWrite(MOT_R_ENA, 157);
			/*
			if(cap-consigne < 40)
			{
				analogWrite(MOT_L_ENA, 255 - (int)(( Compass_deltaCap(cap, consigne) )*2.55));
				analogWrite(MOT_R_ENA, 255);
			}
			else
			{
				analogWrite(MOT_L_ENA, 153);
				analogWrite(MOT_R_ENA, 255);
			}
			*/
		}
		// Sinon on fait tourner a la meme vitesse les deux moteurs
		else
		{
#ifdef _DEBUG_
			Serial.print(F("\tDeltaCap = 0:\t"));
			Serial.println(Compass_deltaCap(cap, consigne));
#endif

			analogWrite(MOT_R_ENA, 255);
			analogWrite(MOT_L_ENA, 255);
		}
	}
	
	// On arrete le robot
	Robot_arret();
}

// AvancerTmp() -- ok
// Faire avancer le robot dans une direction pendant une durée
void Robot_avancerTmp(int consigne, int seconds)
{
	long time;
	char buffer[30];
	
	// On affiche ce qu'on va faire sur la Casio
	sprintf(buffer, " Avancer cap %d pendant %d\n", consigne, seconds);
	Casio_write(buffer);
	
	// On initialise la variable du temps
	time = millis();
	
	// Tant que le temps depis lequel on est dans la fonction
	// est inferieur a la consigne
	while(millis()-time < 1000*seconds)
	{
		/* Delta de commande des moteurs
		
			-Si distance a l'objectif faible
				Ralentir
			-Si cap < consigne
				robot trop a droite
				accelerer droite
				ralentir gauche
			-Si cap > consigne
				robot trop a gauche
				accelerer gauche
				ralentir droite
				
		*/
		int cap = 0;
		cap = Compass_getCap();
		
#ifdef _DEBUG_
			Serial.print(F("\tConsigne:\t")); Serial.print(consigne);
			Serial.print(F("\tCap:\t")); Serial.print(cap);
			Serial.print(F("\tDeltaCap:\t")); Serial.print(Compass_deltaCap(cap, consigne));
			Serial.print(F("\tDeltaCap<0:\t")); Serial.print(Compass_deltaCap(cap, consigne) < 0);
			Serial.print(F("\tDeltaCap>0:\t")); Serial.print(Compass_deltaCap(cap, consigne) > 0);
			Serial.print(F("\tDistance:\t")); Serial.print(1000*seconds - (millis()-time));
			Serial.println();
#endif
		
		// On fait tourner les deux moteurs dans le bon sens
		// (modification de l'etat des broches du hacheur)
		Motor_setSens(MOT_NEGATIVE, MOT_NEGATIVE, MOT_BREAK);
		
		// Si l'ecart entre le cap et la consigne est negatif
		//	Alors on tourne vers la gauche (ralentissement moteur gauche)
		if(Compass_deltaCap(cap, consigne) < 0)
		{
#ifdef _DEBUG_
			Serial.print(F("\tDeltaCap < 0:\t"));
			Serial.println(Compass_deltaCap(cap, consigne));
#endif
			
			analogWrite(MOT_L_ENA, 157);
			analogWrite(MOT_R_ENA, 255);
		}
		// Sinon si l'ecart entre le cap et la consigne est positif
		//	Alors on tourne vers la droite (ralentissement moteur droit)
		else if(Compass_deltaCap(cap, consigne) > 0)
		{
#ifdef _DEBUG_
			Serial.print(F("\tDeltaCap > 0:\t"));
			Serial.println(Compass_deltaCap(cap, consigne));
#endif
			
			analogWrite(MOT_L_ENA, 255);
			analogWrite(MOT_R_ENA, 157);
			/*
			if(cap-consigne < 40)
			{
				analogWrite(MOT_L_ENA, 255 - (int)(( Compass_deltaCap(cap, consigne) )*2.55));
				analogWrite(MOT_R_ENA, 255);
			}
			else
			{
				analogWrite(MOT_L_ENA, 153);
				analogWrite(MOT_R_ENA, 255);
			}
			*/
		}
		// Sinon on fait tourner a la meme vitesse les deux moteurs
		else
		{
#ifdef _DEBUG_
			Serial.print(F("\tDeltaCap = 0:\t"));
			Serial.println(Compass_deltaCap(cap, consigne));
#endif

			analogWrite(MOT_R_ENA, 255);
			analogWrite(MOT_L_ENA, 255);
		}
	}
	
	// On arrete le robot
	Robot_arret();
}

// Tourner() -- ok
// Tourner face a uen direction sur place ou autour d'une roue
void Robot_tourner(int consigne, int mode)
{
	int cap, delta;
	char buffer[25];
	
	/*
		Mode 1:
			-tourner sur place
		Mode 2:
			-tourner autour roue inter
	*/
	
	// On remonte la grille
	Robot_grille(GRI_MONTER);

	// On affiche sur la Casio ce qu'on fait
	sprintf(buffer, " Tourner au cap %d\n", consigne);
	Casio_write(buffer);
	
#ifdef _DEBUG_
	Serial.print(F("# Tourner sur place:\t"));
	Serial.println(consigne);
#endif
	
	// On fait tourner les moteurs au ralenti
	analogWrite(MOT_R_ENA, 153);
	analogWrite(MOT_L_ENA, 153);
	
	// Tant que l'ecart entre la consigne et le cap est different de 0
	do
	{
		// On mesure le cap et on calcule le delta
		cap = Compass_getCap();
		delta = Compass_deltaCap(cap, consigne);
		
#ifdef _DEBUG_
		Serial.print(F("\tCap:\t"));		Serial.print(cap);
		Serial.print(F("\tDelta:\t"));		Serial.println(delta);
#endif
		
		// Si le delta est superieur a 1
		//	Alors on tourne a droite
		if(delta > 1)
			Motor_setSens((mode == 1 ? MOT_POSITIVE : MOT_BREAK), MOT_NEGATIVE, MOT_BREAK);
		
		// Sinon si le delta est inferieur a 1
		//	Alors on tourne a gauche
		else if(delta < -1)
			Motor_setSens(MOT_NEGATIVE, (mode == 1 ? MOT_POSITIVE : MOT_BREAK), MOT_BREAK);
		
	}while(delta<-1 || delta>1);
	
	// On s'arrete de tourner
	Robot_arret();
	
	// On redescend la grille
	Robot_grille(GRI_DESCENDRE);
}

// Grille() -- ok
// Faire monter ou descendre la grille
void Robot_grille(int value)
{
	// Monter
	if(value == GRI_MONTER)
	{
		// On affiche sur la casio ce qu'on fait
		Casio_write_P(PSTR(" Monter la grille\n"));
		
		// On demande a la fonction setSens de faire tourner le moteur de la grille dans le sens positif
		Motor_setSens(MOT_BREAK, MOT_BREAK, MOT_POSITIVE);
		
		// On met la broche du hacheur a 153 (valeur minimale) - non utilisé car pas de PWM
		analogWrite(MOT_G_ENA, 153);
		
		// Tant que le fin de course n'est pas au niveau haut on reste dans la boucle
		while(!digitalRead(PIN_FC_H))
			delay(1);
		
		// On est sorti de la boucle -> on arrete le moteur
		Robot_arret();
	}
	// Descendre
	else if(value == GRI_DESCENDRE)
	{
		// On affiche sur la casio ce qu'on fait
		Casio_write_P(PSTR(" Descendre la grille\n"));
		
		// On demande a la fonction setSens de faire tourner le moteur de la grille dans le sens negatif
		Motor_setSens(MOT_BREAK, MOT_BREAK, MOT_NEGATIVE);
		
		// On met la broche du hacheur a 153 (valeur minimale) - non utilisé car pas de PWM
		analogWrite(MOT_G_ENA, 153);
		
		// Tant que le fin de course n'est pas au niveau haut on reste dans la boucle
		while(!digitalRead(PIN_FC_B))
			delay(1);
		
		// On est sorti de la boucle -> on arrete le moteur
		Robot_arret();
	}
}

// Arret() -- ok
// Arreter tout les moteurs
void Robot_arret()
{
	Motor_setSens(MOT_BREAK, MOT_BREAK, MOT_BREAK);
	
	analogWrite(MOT_R_ENA, 0);
	analogWrite(MOT_L_ENA, 0);
	analogWrite(MOT_G_ENA, 0);
}

