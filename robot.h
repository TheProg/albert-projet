/*****************************************************/
/** 	File:		Robot.h							**/
/** 	Project:	Albert							**/
/** 	Author:		DDesclaux TFel					**/
/** 	Description: Functions for robot control	**/
/** 	State:		Clean							**/
/*****************************************************/

#ifndef _ROBOT_H_
#define _ROBOT_H_
	
	#include <Arduino.h>
	
	#include "define.h"
	#include "motor.h"
	#include "casio.h"
	#include "compass.h"
	
	void Robot_avancer(int consigne, int length);
	void Robot_avancerTmp(int consigne, int seconds);
	void Robot_tourner(int consigne, int mode);
	void Robot_grille(int value);
	void Robot_arret();
	
#endif

