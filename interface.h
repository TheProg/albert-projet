/*****************************************************/
/** 	File:		Interface.h						**/
/** 	Project:	Albert							**/
/** 	Author:		DDesclaux TFel					**/
/** 	Description: Functions for interfaces		**/
/** 	State:		To do							**/
/*****************************************************/

#ifndef _INTERFACE_H_
#define _INTERFACE_H_
	
	#include <Arduino.h>
	#include <IRremote.h>
	#include <string.h>
	
	#include "define.h"
	#include "robot.h"
	#include "motor.h"
	#include "casio.h"
	
	#define OEM_REMOTE
	#define DELAY	10
	
	void Interface_IR();
	void Interface_BT();
	void Interface_BTEvent();
	void Interface_BTSend();
	
#endif

