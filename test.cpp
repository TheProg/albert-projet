/*****************************************************/
/** 	File:		Test.c							**/
/** 	Project:	Albert							**/
/** 	Author:		DDesclaux TFel					**/
/** 	Description: Functions for tests			**/
/** 	State:		ok								**/
/*****************************************************/

#include "test.h"

// Demo() -- ok
// Faire une petite demo des capacités du robot
void Test_demo()
{
	int cap = Compass_getCap();
	
	// On affiche sur la casio ce qu'on fait
	Casio_clear();
	Casio_write_P(PSTR("# Debut demo #"));
	
	Robot_avancerTmp(cap, 7);
	// Virage 90 G
	Robot_tourner(cap+90, 1);
	Robot_avancerTmp(cap+90, 3);
	// Virage 90 G
	Robot_tourner(cap+180, 1);
	Robot_avancerTmp(cap+180, 7);
	// Virage 90 D
	Robot_tourner(cap+90, 1);
	Robot_avancerTmp(cap+90, 3);
	// Virage 90 D
	Robot_tourner(cap, 1);
	Robot_avancerTmp(cap, 7);
	// Virage 90 D
	Robot_tourner(cap-90, 0);
	Robot_avancerTmp(cap-90, 3);
	// Virage 90 D
	Robot_tourner(cap-180, 0);
	Robot_avancerTmp(cap-180, 7);
	// Virage 90 G
	Robot_tourner(cap-90, 0);
	Robot_avancerTmp(cap-90, 3);
	// Virage 90 G
	Robot_tourner(cap, 0);
	Robot_avancerTmp(cap, 7);
	
	// On affiche sur la Casio qu'on a fini
	Casio_write_P(PSTR("# Fin demo #"));
}

// Compass() -- ok
// Test de la boussole
void Test_compass()
{
	char buffer[25];
	int nbrValues = 10;
	
	Casio_clear();
	Casio_write_P(PSTR("Test de la bousolle.\n"));
	
	Casio_write_P(PSTR(" Nbr de valeurs:"));
	nbrValues = atoi(Casio_read(0));
	Casio_write_P(PSTR("\n"));
	delay(1000);
	
	nbrValues %= 999;
	for(int i=0; i<nbrValues; i++)
	{
		sprintf(buffer, "Cap %03d/%03d: %03d deg\n", i, nbrValues, (int)Compass_getCap());
		Casio_write(buffer);
		delay(1500);
	}
	Casio_write_P(PSTR("\n## FIN TEST ##\n\n"));
	delay(1000);
}

// CompteTour() -- ok
// Test du compte-tour
void Test_compteTour()
{
	char buffer[25];
	int nbrTurns = 10;
	int i = 0;
	
	Casio_clear();
	Casio_write_P(PSTR("Test du compte tour.\n"));
	
	Casio_write_P(PSTR(" Nbr de tours:"));
	nbrTurns = atoi(Casio_read(0));
	Casio_write_P(PSTR("\n"));
	delay(1000);
	
	nbrTurns %= 999;
	while(i < nbrTurns)
	{
		while(!digitalRead(PIN_CT));
		while(digitalRead(PIN_CT));
		i++;
		sprintf(buffer, "Tour %03d/%03d\n", i, nbrTurns);
		Casio_write(buffer);
	}
	Casio_write_P(PSTR("\n## FIN TEST ##\n\n"));
	delay(1000);
}

// FinCourse() -- ok
// Test des fins de course
void Test_finCourse()
{
	int nbrAppuis = 10;
	int i = 0;
	
	Casio_clear();
	Casio_write_P(PSTR("Test fins de course.\n"));
	
	Casio_write_P(PSTR(" Nbr d'appuis:"));
	nbrAppuis = atoi(Casio_read(0));
	Casio_write_P(PSTR("\n"));
	delay(1000);
	
	nbrAppuis %= 999;
	while(i < nbrAppuis)
	{
		if(digitalRead(PIN_FC_H))
		{
			while(digitalRead(PIN_FC_H));
			i++;
			Casio_write_P(PSTR("Haut !\n"));
		}
		if(digitalRead(PIN_FC_B))
		{
			while(digitalRead(PIN_FC_B));
			i++;
			Casio_write_P(PSTR("Bas !\n"));
		}
	}
	Casio_write_P(PSTR("\n## FIN TEST ##\n"));
	delay(1000);
}

// Motors() -- ok
// Test des moteurs
void Test_motors()
{
	int idMotor = 0;
	int i = 0;
	
	Casio_clear();
	Casio_write_P(PSTR("Test des moteurs.\n"));
	
	Casio_write_P(PSTR(" 1 - Moteur droit\n"));
	Casio_write_P(PSTR(" 2 - Moteur gauche\n"));
	Casio_write_P(PSTR(" 3 - Moteur grille\n"));
	
	idMotor = atoi(Casio_read(0));
	
	if(idMotor == MOTOR_R)
	{
		Casio_write_P(PSTR("\nTest Moteur Droit\n"));
		// Horaire
		Casio_write_P(PSTR(" Sens Horaire\n"));
		Motor_setSens(MOT_POSITIVE, MOT_BREAK, MOT_BREAK);
		Test_motor(MOT_R_ENA);
		
		// Anti-Horaire
		Casio_write_P(PSTR(" Sens Anti-Horaire\n"));
		Motor_setSens(MOT_NEGATIVE, MOT_BREAK, MOT_BREAK);
		Test_motor(MOT_R_ENA);
	}
	else if(idMotor == MOTOR_L)
	{
		Casio_write_P(PSTR("\nTest Moteur Gauche\n"));
		// Horaire
		Casio_write_P(PSTR(" Sens Horaire\n"));
		Motor_setSens(MOT_BREAK, MOT_POSITIVE, MOT_BREAK);
		Test_motor(MOT_L_ENA);
		
		// Anti-Horaire
		Casio_write_P(PSTR(" Sens Anti-Horaire\n"));
		Motor_setSens(MOT_BREAK, MOT_NEGATIVE, MOT_BREAK);
		Test_motor(MOT_L_ENA);
	}
	else
	{
		Casio_write_P(PSTR("\nTest Moteur Grille\n"));
		// Horaire
		Robot_grille(GRI_MONTER);
		
		// Anti-Horaire
		Robot_grille(GRI_DESCENDRE);
	}
	
	Casio_write_P(PSTR("\n## FIN TEST ##\n"));
}

// Motor() -- ok
// Fait varier la vitese d'un moteur
void Test_motor(int pinEna)
{
	for(int j=153; j<255; j++)
	{
		analogWrite(pinEna, j);
		delay(50);
	}
	for(int j=255; j>153; j--)
	{
		analogWrite(pinEna, j);
		delay(50);
	}
	analogWrite(pinEna, 0);
}

